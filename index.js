/**
 * Something cool is supposed to happen here
 */
var express = require('express'),
	app		= express(),
	router	= express.Router()

app.use(express.static('build'))

app.get('*', function(req, res){
	res.sendfile('./build/index.html')
})

app.listen(6969)