/**
 * Dependencies
 */
var	gulp 		= require('gulp'),
	browserify 	= require('browserify')
	transform 	= require('vinyl-transform')
	source 		= require('vinyl-source-stream')
	uglify 		= require('gulp-uglify')
	watchify 	= require ('watchify')
	gutil 		= require('gulp-util')
	fs 			= require('fs')
	browserSync = require('browser-sync')
	nodemon 	= require('gulp-nodemon')
	sass 		= require('gulp-sass')


/**
 * Configuration
 */
var dirSource	= './src/client'
	dirBuild 	= './build'
	jsMain		= dirSource + '/js/client.js'


/**
 * Internal
 */
var customOpts = watchify.args
customOpts.entries = [jsMain]

var bundler = watchify(browserify(customOpts))
bundler.transform('brfs')


/**
 * Bundle JS files
 */
function bundle(){
	console.log('bundling..')
	return bundler.bundle()
		.on('error', gutil.log.bind(gutil, 'Browserify Error'))
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('./build/js'))
}

/**
 * Restart server when neccesary
 */
gulp.task('nodemon', function(cb) {
	var started = false

	return nodemon({
		script: './index.js',
		ignore: ['build/js/', 'client/js/']
	}).on('start', function() {
		// to avoid nodemon being started multiple times
		// thanks @matthisk
		if (!started) {
			cb()
			started = true
		}
	})
})

/**
 * Compile SASS and pipe to BrowserSync
 */
gulp.task('sass', function() {
	return gulp.src(dirSource + "/scss/*.scss").pipe(sass()).pipe(gulp.dest("./build/css")).pipe(browserSync.stream())
})

/**
 * BrowserSync
 */
gulp.task('browser-sync', ['nodemon'], function() {
	browserSync.init(null, {
		proxy: "http://localhost:6969",
		files: ["build/**/*.*"],
		port: 80
	})
})

/**
 * Development mode
 */
gulp.task('serve', ['sass', 'nodemon', 'browser-sync'], function(){
	gulp.watch(dirSource + '/scss/**', ['sass'])
	gulp.watch(dirBuild + '/**').on('change', browserSync.reload)
	gulp.watch(jsMain).on('change', bundle)
	bundler.on('update', bundle) // on any dep update, runs the bundler

})
gulp.task('default', ['serve'])
gulp.task('js', bundle)