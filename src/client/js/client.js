/*====================================
=            Dependencies            =
====================================*/
riot = require('riot/riot+compiler')


/*==============================
=            States            =
==============================*/
riot.route('/page', function(){
	riot.mount('module-template', 'state-dev-page')
})

// 404
riot.route('/*', function(){
	riot.mount('module-template', 'state-error-notfound')
})

// home
riot.route(function(){
	riot.mount('module-template', 'state-home')
})


/*=====================================
=            Configuration            =
=====================================*/

/**
 * Start the framework
 */
riot.compile(require('fs').readFileSync('./src/client/html/modules.html', 'utf8'))
riot.route.base('/')
riot.route.start(true)
